//
//  TabCoordinator.swift
//  oVK
//
//  Created by Pavel Belov on 24.10.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

enum TabBarPage {
    case news
    case personal
    case contacts
    
    init? (index: Int) {
        switch index {
        case 0:
            self = .personal
        case 1:
            self = .news
        case 2:
            self = .contacts
        default:
            return nil
        }
    }
    
    func pageTitleValue() -> String {
        switch self {
        case .news:
            return "News"
        case .personal:
            return "Profile"
        case .contacts:
            return "Contacts"
        }
    }
    
    func pageOrderNumber() -> Int{
        switch self {
        case .personal:
            return 0
        case .news:
            return 1
        case .contacts:
            return 2
        }
    }
}

