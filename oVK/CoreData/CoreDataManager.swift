//
//  CoreDataManager.swift
//  oVK
//
//  Created by Pavel Belov on 18.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import VK_ios_sdk

class CoreDataManager {
    
    static let shared = CoreDataManager()
    var presentFriendsDelegate: FriendsProtocol?
    
    enum Whois: String {
        case me = "me"
        case friend = "friend"
    }
    
    
    func loadFriends(who: Whois) -> [NSManagedObject]  {
       DispatchQueue.main.sync {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let context = appDelegate.persistentContainer.viewContext
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "UserDetail")
            let predicate = NSPredicate(format: "whois == %@", who.rawValue)
            fetchRequest.predicate = predicate
            
            do {
                let res = try context.fetch(fetchRequest) as! [NSManagedObject]
                self.presentFriendsDelegate?.modelsArray = res
                return res
                
            } catch let error as NSError {
                return []
            }
       }
    }
    
    func saveUsers(_ usersArray: [UserVK], whois: Whois) {
        DispatchQueue.main.async {
            let appDelegate = UIApplication.shared.delegate as! AppDelegate
            let managedContext = appDelegate.persistentContainer.viewContext
            let entity = NSEntityDescription.entity(forEntityName: "UserDetail", in: managedContext)!
            let fetchRequest: NSFetchRequest<NSFetchRequestResult> = NSFetchRequest(entityName: "UserDetail")
            let predicate = NSPredicate(format: "whois == %@", whois.rawValue)
            fetchRequest.predicate = predicate
            let delRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            do {
                try managedContext.execute(delRequest)
            } catch {
                //
            }
            
            for user in usersArray {
                
                let userObject: NSManagedObject = NSManagedObject(entity: entity, insertInto: managedContext)
               
                if let id = user.id as Int? {
                    userObject.setValue(id, forKey: "id")
                    //print(id)
                }
                if let lastName = user.last_name as String? {
                    userObject.setValue(lastName, forKey: "last_name")
                   // print(lastName)
                }
                if let firstName = user.first_name as String? {
                    userObject.setValue(firstName, forKey: "first_name")
                }
                if let books = user.books as String? {
                    userObject.setValue(books, forKey: "books")
                }
                if let photo_50 = user.photo_50 as String? {
                    userObject.setValue(photo_50, forKey: "photo_50")
                }
                if let photo_200 = user.photo_200 as String? {
                    userObject.setValue(photo_200, forKey: "photo_200")
                }
                if let city = user.city?.title as String? {
                    userObject.setValue(city, forKey: "city")
                }
                if let online = user.online as Int? {
                    userObject.setValue(online, forKey: "online")
                }
                if let graduation = user.graduation as Int? {
                    userObject.setValue(graduation, forKey: "graduation")
                }
                if let followers = user.followers_count as Int? {
                    userObject.setValue(followers, forKey: "followers")
                }
                if let university_name = user.university_name as String? {
                    userObject.setValue(university_name, forKey: "university_name")
//                    if whois == .me {
//                        print(university_name)
//                    } else {
//                        print("NOTME")
//                    }

                    // print(university_name)
                }
                
                userObject.setValue(whois.rawValue, forKey: "whois")
            
                
                guard managedContext.hasChanges else { return }
                do {
                    try managedContext.save()
                } catch let error as NSError {
                    print("ERROR \(error)")
                }
                
            }
        }
        
    }
    

}
