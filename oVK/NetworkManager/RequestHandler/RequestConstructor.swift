//
//  RequestConstructor.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

final class RequestConstructor {
    
    static func ReturnBaseRequest( _ url: URL, method: RequestMethod, header: [String: String] = [:]) -> URLRequest? {
        
        var request: URLRequest?
        
        request = URLRequest(url: url)
        request?.httpMethod = method.rawValue
        request?.allHTTPHeaderFields = header
        
        guard request != nil else { return nil }
        return request
    }
}
