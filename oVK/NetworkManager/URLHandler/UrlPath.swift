//
//  UrlPath.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

enum UrlPath: String {
    case authPath = "/login"
    case usersGet = "/method/users.get"
    case friendsGet = "/method/friends.get"
}
