//
//  URLConstructor.swift
//  oVK
//
//  Created by Pavel Belov on 17.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

enum UrlConstructor  {
    
    case authUrl, getUsers, getFriends
    
    func urls(_ query: [URLQueryItem] = []) -> Result<URL, UrlError> {
        switch self {
        case .authUrl:
            var urlComponent = URLComponents()
            urlComponent.scheme = UrlScheme.http.rawValue
            urlComponent.host = UrlHost.auth.rawValue
            urlComponent.path = UrlPath.authPath.rawValue
            urlComponent.queryItems = query
            guard urlComponent.url != nil else { return .failure(.invalidURL) }
            return .success(urlComponent.url!)
            
        case .getUsers:
            var urlComponent = URLComponents()
            urlComponent.scheme = UrlScheme.https.rawValue
            urlComponent.host = UrlHost.api.rawValue
            urlComponent.path = UrlPath.usersGet.rawValue
            urlComponent.queryItems = query
            guard urlComponent.url != nil else { return .failure(.invalidURL) }
            return .success(urlComponent.url!)
            
        case .getFriends:
            var urlComponent = URLComponents()
            urlComponent.scheme = UrlScheme.https.rawValue
            urlComponent.host = UrlHost.api.rawValue
            urlComponent.path = UrlPath.friendsGet.rawValue
            urlComponent.queryItems = query
            guard urlComponent.url != nil else { return .failure(.invalidURL) }
            return .success(urlComponent.url!)
            
        }
    }
}
