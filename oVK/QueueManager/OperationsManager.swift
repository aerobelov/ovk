//
//  OperationsManager.swift
//  oVK
//
//  Created by Pavel Belov on 25.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation
import CoreData


class OperationsManager {
    
    static var shared = OperationsManager()
    static var friends = [NSManagedObject]()
    static var me = [NSManagedObject]()
    
    //GET FRIENDS FROM VK API
    let downloadOperation = GetVKFriendsOperation()
    
    //SAVE VK MANAGER FRIENDS TO COREDATA
    let saveToCoreOperation = BlockOperation {
        CoreDataManager.shared.saveUsers(VKManager.shared.friends, whois: .friend)
    }
    
    //LOAD FRIENDS FROM COREDATA
    let loadFromCoreOperation = BlockOperation {
        OperationsManager.friends = CoreDataManager.shared.loadFriends(who: .friend)
    }
    
    
    func getFriends(_ callback: (([NSManagedObject])->())?) {
        
        let friendsQueue = OperationQueue()
        
        //OPERATION ORDER
        saveToCoreOperation.addDependency(downloadOperation)
        loadFromCoreOperation.addDependency(saveToCoreOperation)
        
        friendsQueue.addOperations([downloadOperation, saveToCoreOperation, loadFromCoreOperation], waitUntilFinished: false)
        
        //WHEN LOAD COMPLETED - RUN CALLBACK
        loadFromCoreOperation.completionBlock = {
            if let callback = callback {
                callback(OperationsManager.friends)
            }
           
        }   
    }
    
    func getMyProfile(_ callback: (([NSManagedObject])->())?) {

        let queue = OperationQueue()
        let getF = GetPersonalOperation()
        let save = BlockOperation {
            CoreDataManager.shared.saveUsers(VKManager.shared.personalProfile, whois: .me)
        }
        let load = BlockOperation {
            OperationsManager.me = CoreDataManager.shared.loadFriends(who: .me)
        }
        save.addDependency(getF)
        queue.addOperations([getF, save, load], waitUntilFinished: false)
        load.completionBlock = {
            if let callback = callback {
                callback(OperationsManager.me)
            }
           
        }

    }
    
    
}
