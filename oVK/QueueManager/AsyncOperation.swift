//
//  AsyncOperation.swift
//  oVK
//
//  Created by Pavel Belov on 27.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

class AsyncOperation: Operation {
    
    // Определяем перечисление enum State со свойством keyPath
    enum State: String {
        case ready, executing, finished
        
        fileprivate var keyPath: String {
            return "is" + rawValue.capitalized
        }
    }
    
    var state = State.ready {
        willSet {
            willChangeValue(forKey: newValue.keyPath)
            willChangeValue(forKey: state.keyPath)
        }
        didSet {
            didChangeValue(forKey: oldValue.keyPath)
            didChangeValue(forKey: state.keyPath)
        }
    }
    
    override var isReady: Bool {
        return super.isReady && state == .ready
    }
    
    override var isExecuting: Bool {
        return state == .executing
    }
    
    override var isFinished: Bool {
        return state == .finished
    }
    
    private var _isCanceled = false {
        willSet {
            willChangeValue(forKey: "isCancelled")
        }
        didSet {
            didChangeValue(forKey: "isCancelled")
        }
    }
    
    override var isCancelled: Bool {
        get {
            return _isCanceled
        }
    }
    
    override var isAsynchronous: Bool {
        return true
    }
    
    override func start() {
        guard isCancelled == false else {
            state = .finished
            return
        }
        main()
        state = .executing
    }
    
    override func cancel() {
        _isCanceled = true
    }
}
