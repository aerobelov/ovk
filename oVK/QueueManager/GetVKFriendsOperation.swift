//
//  GetVKFriendsOperation.swift
//  oVK
//
//  Created by Pavel Belov on 27.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

class GetVKFriendsOperation: AsyncOperation {
    
    override init() {
        super.init()
    }
    
    override func main() {
        VKManager.shared.getF {
            self.state = .finished
        }
    }
    
}
