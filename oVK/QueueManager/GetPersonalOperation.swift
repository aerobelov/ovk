//
//  GetPersonalOperation.swift
//  oVK
//
//  Created by Pavel Belov on 05.12.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

class GetPersonalOperation: AsyncOperation {
    
    override init() {
        super.init()
    }
    
    override func main() {
        VKManager.shared.getPersonal {
            self.state = .finished
        }
    }
    
}
