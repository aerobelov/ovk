////
////  SaveFriendsOperation.swift
////  oVK
////
////  Created by Pavel Belov on 30.11.2020.
////  Copyright © 2020 Pavel Belov. All rights reserved.
////
//
//import Foundation
//import UIKit
//import CoreData
//
//class SaveUsersOperaion: AsyncOperation {
//    
//    let entity = NSEntityDescription.entity(forEntityName: "UserDetail", in: CoreDataManager.managedContext)!
//    let fetchRequest: NSFetchRequest<NSFetchRequestResult>?
//    let deleteRequest: NSBatchDeleteRequest?
//    
//    override init() {
//        self.fetchRequest = NSFetchRequest(entityName: "UserDetail")
//        self.deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest!)
//    }
//    
//    do {
//        try CoreDataManager.managedContext.execute(deleteRequest)
//    } catch {
//        print ("CANT DELETE")
//    }
//        
//        for user in usersArray {
//            let userObject = NSManagedObject(entity: entity, insertInto: managedContext)
//            
//            if let lastName = user.last_name as String? {
//                userObject.setValue(lastName, forKey: "last_name")
//            }
//            if let firstName = user.first_name as String? {
//                userObject.setValue(firstName, forKey: "first_name")
//            }
//            if let books = user.books as String? {
//                userObject.setValue(books, forKey: "books")
//            }
//            if let photo_50 = user.photo_50 as String? {
//                userObject.setValue(photo_50, forKey: "photo_50")
//            }
//            if let photo_200 = user.photo_200 as String? {
//                userObject.setValue(photo_200, forKey: "photo_200")
//            }
//            if let city = user.city?.title as String? {
//                userObject.setValue(city, forKey: "city")
//            }
//            if let online = user.online as Int? {
//                userObject.setValue(online, forKey: "online")
//            }
//            if let graduation = user.education?.graduation as Int? {
//                userObject.setValue(graduation, forKey: "graduation")
//            }
//            if let followers = user.followers_count as Int? {
//                userObject.setValue(followers, forKey: "followers")
//            }
//            print(user.education ?? "NO EDU")
//            if let university_name = user.education?.university_name as String? {
//                userObject.setValue(university_name, forKey: "university_name")
//            }
//            switch whois {
//            case .me:
//                userObject.setValue(whois.rawValue, forKey: "whois")
//            case .friend:
//                userObject.setValue(whois.rawValue, forKey: "whois")
//            }
//            
//            guard managedContext.hasChanges else { return }
//            do {
//                try managedContext.save()
//            } catch let error as NSError {
//                print("ERROR \(error)")
//            }
//            
//        }
//    
//}
