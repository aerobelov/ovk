//
//  PersonalProfile.swift
//  oVK
//
//  Created by Pavel Belov on 31.10.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

struct PersonalProfile {
    var lastName: String?
    var givenName: String?
    var socialStatus: String = ""
    var networkStatus: String = ""
    var subscribersCount: Int = 0
    var organization: String = ""
    var city: String = ""
    var education: String = ""
    var friendsCount: Int = 0
    var profileImageFileName: String = ""

    init(lastName: String, givenName: String, city: String, education: String) {
        self.lastName = lastName
        self.givenName = givenName
        self.city = city
        self.education = education
        self.profileImageFileName = "profile.jpg"
    }
}


struct City: Decodable {
    var title: String?
    var id: Int?
}
struct Education: Decodable {
    var graduation: Int?
    var university_name: String?
}


struct UserVK: Decodable {
    var id: Int?
    var last_name: String?
    var first_name: String?
    var photo_50: String?
    var photo_200: String?
    var books: String?
    var followers_count: Int?
    var online: Int?
    var is_closed: Bool?
    
    var city: City?
    var graduation: Int?
    var university_name: String?
}

struct Personal: Decodable {
    var id: Int?
    var last_name: String?
    var first_name: String?
    var photo_50: String?
    var photo_200: String?
    var books: String?
    var followers_count: Int?
    var online: Int?
    var is_closed: Bool?
   
    var city: City?
    var graduation: Int?
    var university_name: String?
}

struct Users: Decodable {
    var response: [UserVK]?
}

struct Profile: Decodable {
    var response: [Personal]?
}

struct Response: Decodable {
    var count: Int?
    var items: [UserVK]
}

struct FriendsList: Decodable {
    var response: Response?
}




