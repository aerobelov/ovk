//
//  PersonalToVKConverter.swift
//  oVK
//
//  Created by Pavel Belov on 12.12.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation

extension VKManager {
    func convToVk(user: Personal) -> UserVK {
        var tempUser = UserVK()
        tempUser.id = user.id
        tempUser.last_name = user.last_name
        tempUser.first_name = user.first_name
        tempUser.photo_50 = user.photo_50
        tempUser.photo_200 = user.photo_200
        tempUser.books = user.books
        tempUser.city = user.city
        tempUser.is_closed = user.is_closed
        tempUser.followers_count = user.followers_count
        tempUser.online = user.online
        print(user.university_name)
        tempUser.graduation = user.graduation
        
        tempUser.university_name = user.university_name
        print(tempUser.university_name)
        return tempUser
    }
}
