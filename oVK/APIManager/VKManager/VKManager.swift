//
//  VKManager.swift
//  oVK
//
//  Created by Pavel Belov on 10.11.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation
import VK_ios_sdk
import OAuthSwift

class VKManager: NSObject, VKSdkDelegate, VKSdkUIDelegate {
    
    static let shared = VKManager()
    private let vkId = "7656864"
    private let vkSecret = "O8u57BxrEctlU46drQDf"
    private let scope = ["email", "offline", "friends", "photo_50"]
    private var token = ""
    private var userId = 0
    weak var completeDelegate: VKAuthorizedProtocol? = nil
    weak var presentDelegate: LoginCoordinatorProtocol? = nil
    var updater: (() -> Void)?
    
    private var instance: VKSdk? = nil
    var friends: [UserVK] = []
    var personalProfile: [UserVK] = []

    
    override init() {
        super.init()
        self.setup()
    }
    
    func setup() {
        instance = VKSdk.initialize(withAppId: vkId)
        instance?.register(self)
        instance?.uiDelegate = self
        VKSdk.forceLogout()
    }
    
    func authorize() {
        VKSdk.wakeUpSession(scope) { [weak self] (state, error) in
            guard let self = self else { return }
            self.instance?.uiDelegate = self
            if state == .authorized {
                self.completeDelegate?.authorized()
            } else {
                if VKSdk.vkAppMayExists() {
                    VKSdk.authorize(self.scope, with: .disableSafariController)
                } else {
                    VKSdk.authorize(self.scope, with: .unlimitedToken)
                }
            }
            
        }
    }

    

    
    func getF (_ completion: ( () -> () )? ) {
       
            let api_fields = ["id", "photo_50", "photo_200", "books", "followers_count", "education", "online", "city" ]
            let user = URLQueryItem(name: "user_id", value: String(self.userId))
            let fields = URLQueryItem(name: "fields", value: api_fields.joined(separator: ","))
            let token = URLQueryItem(name: "access_token", value: self.token)
            let version = URLQueryItem(name: "v", value: "5.126")
            let session = SessionManager([:], .post, [user, fields, token, version], .getFriends)
       // print (session.url)
            session.downloadData { result in
                
                switch result {
                case .success(let data):
                    if let reply = try? JSONDecoder().decode(FriendsList.self, from: data) {
                        if let response = reply.response {
                            self.friends = response.items
                        }
                    }
                    completion?()
                case .failure(let error):
                    print(error)
                }
            }
    }
    
    func getPersonal(_ completion: ( () -> () )? )  {
        let api_fields = ["id", "photo_50", "photo_200", "books", "followers_count", "education", "online", "city" ]
        let users = URLQueryItem(name: "user_ids", value: String(self.userId))
        let fields = URLQueryItem(name: "fields", value: api_fields.joined(separator: ","))
        let token = URLQueryItem(name: "access_token", value: self.token)
        let version = URLQueryItem(name: "v", value: "5.126")
        let session = SessionManager([:], .post, [users, fields, token, version], .getUsers)
        session.downloadData { result in
            
            switch result {
            case .success(let data):
                if let reply = try? JSONDecoder().decode(Profile.self, from: data) {
                    if let response = reply.response {
                        self.personalProfile.append(self.convToVk(user: response[0]))
                    }
                }
                completion?()
            case .failure(let error):
                print(error)
            }
        }
   
    }
    
    func vkSdkAccessAuthorizationFinished(with result: VKAuthorizationResult!) {
        if let token = result.token, let userId = token.userId {
            self.token = token.accessToken
            self.userId = Int(userId) ?? 0
            self.completeDelegate?.authorized()
        }
    }
    
    func vkSdkUserAuthorizationFailed() {
        self.completeDelegate?.notAuthorized()
    }
    
    func vkSdkShouldPresent(_ controller: UIViewController!) {
        self.presentDelegate?.showAuthController(with: controller)
    }
    
    func vkSdkNeedCaptchaEnter(_ captchaError: VKError!) {
        //
    }
    
}

protocol VKAuthorizedProtocol: class {
    func authorized()
    func notAuthorized()
}


protocol LoginCoordinatorProtocol: Coordinator {
    func startVK()
    func showAuthController(with controller: UIViewController)
}

