//
//  Coordinator.swift
//  oVK
//
//  Created by Pavel Belov on 19.10.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator: class {
    var finishDelegate: CoordinatorFinishDelegate? {get set}
    var navigationController: UINavigationController {get set}
    var childCoordinators: [Coordinator] {get set}
    var type: CoordinatorType {get}
    func start()
    func finish()
    
    init(_ navigationController: UINavigationController)
}

extension Coordinator {
    func finish() {
        childCoordinators.removeAll()
        finishDelegate?.coordinatorDidFinish(childCoordinator: self)
    }
}

enum CoordinatorType {
    case app, login, tab
}
