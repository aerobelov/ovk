//
//  CoordinatorsProtocols.swift
//  oVK
//
//  Created by Pavel Belov on 24.10.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation
import UIKit

protocol AppCoordinatorProtocol: Coordinator {
    func showLoginFlow()
    func showMainFlow()
}


protocol CoordinatorFinishDelegate: class {
    func coordinatorDidFinish(childCoordinator: Coordinator)
}

protocol TabCoordinatorProtocol: Coordinator {
    var tabBarController: UITabBarController { get set }
    func selectPage (_ page: TabBarPage)
    func setSelectedIndex (_ index: Int)
    func currentPage () -> TabBarPage
}


