//
//  TabBarCoordinator.swift
//  oVK
//
//  Created by Pavel Belov on 24.10.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import UIKit

class TabCoordinator: NSObject, Coordinator {
    
    weak var finishDelegate: CoordinatorFinishDelegate?
    var childCoordinators: [Coordinator] = []
    var navigationController: UINavigationController
    var tabBarController: UITabBarController
    var type: CoordinatorType { .tab }
    var exitbutton: UIBarButtonItem?
    
    required init (_ navigationController: UINavigationController) {
        self.navigationController = navigationController
        self.tabBarController = UITabBarController()
    }
    
    func start () {
        let pages: [TabBarPage] = [.news, .contacts, .personal]
            .sorted(by: {$0.pageOrderNumber() < $1.pageOrderNumber()})
        let controllers: [UINavigationController] = pages.map({ getTabController($0) })
        prepareTabBarController(withTabBarControllers: controllers)
    }
    
    deinit {
        print("TabC deinit")
    }
    
    private func prepareTabBarController(withTabBarControllers tabControllers: [UIViewController]) {
        tabBarController.delegate = self as UITabBarControllerDelegate
        tabBarController.setViewControllers(tabControllers, animated: false)
        tabBarController.selectedIndex = TabBarPage.personal.pageOrderNumber()
        tabBarController.tabBar.isTranslucent = false
        navigationController.viewControllers = [tabBarController]
        exitbutton = UIBarButtonItem(title: "exit", style: .plain, target: self, action: nil)
        var items = [UIBarButtonItem]()
        navigationController.toolbar.isHidden = false
        items.append(exitbutton!)
        navigationController.toolbar.setItems(items, animated: false)
    }
    
    private func getTabController (_ page: TabBarPage) -> UINavigationController {
        let navController = UINavigationController()
        navController.setNavigationBarHidden(false, animated: false)
        navController.tabBarItem = UITabBarItem.init(title: page.pageTitleValue(), image: nil, tag: page.pageOrderNumber())
        
        switch page {
            case .contacts:
                let contactsVC = ContactsTableViewController()
                contactsVC.didSendEventClosure = { [weak self] event in
                    switch event {
                    case .contacts:
                        print("case personal")
                    case .noname:
                        print("NOname")
                    }
                }
                navController.pushViewController(contactsVC, animated: false)
            case .news:
                let feedVC = FeedTableViewController()
                feedVC.didSendEventClosure = { [weak self] event in
                    switch event {
                    case .caseOne:
                        print("CaseOne")
                    }
                }
                navController.pushViewController(feedVC, animated: false)
            case .personal:
                let personalVC = PersonalViewController()
                personalVC.didSendEventClosure = { [weak self] event in
                    switch event {
                    case .caseOne:
                        print("CasePersonal")
                    }
                }
                navController.pushViewController(personalVC, animated: false)
        }
            

        return navController
        }
    
    func currentPage() -> TabBarPage? {
        TabBarPage.init(index: tabBarController.selectedIndex)
    }
    
    func selectPage (_ page: TabBarPage) {
        tabBarController.selectedIndex = page.pageOrderNumber()
    }
        
    func setSelectedIndex (_ index: Int) {
        guard let page = TabBarPage.init(index: index) else { return }
        tabBarController.selectedIndex = page.pageOrderNumber()
    }



}

extension TabCoordinator: UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("")
    }
}
