//
//  AuthCoordinator.swift
//  oVK
//
//  Created by Pavel Belov on 23.10.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation
import UIKit



class LoginCoordinator: LoginCoordinatorProtocol {

    weak var finishDelegate: CoordinatorFinishDelegate?
    var navigationController: UINavigationController
    var childCoordinators: [Coordinator] = []
    var type: CoordinatorType { .login }
    
    required init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
        VKManager.shared.presentDelegate = self
    }
    
    func start() {
        startVK()
    }
 
    func startVK() {
        VKManager.shared.authorize()
    }
    
    func showAuthController(with controller: UIViewController) {
        navigationController.pushViewController(controller, animated: false)
    }
    
}
