//
//  MainCoordinator.swift
//  oVK
//
//  Created by Pavel Belov on 23.10.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import Foundation
import UIKit



class AppCoordinator: AppCoordinatorProtocol, VKAuthorizedProtocol {
  
    weak var finishDelegate: CoordinatorFinishDelegate? = nil
    var navigationController: UINavigationController
    var childCoordinators = [Coordinator]()
    var type: CoordinatorType { .app }
    
    required init(_ navigationController: UINavigationController) {
        self.navigationController = navigationController
        navigationController.setNavigationBarHidden(true, animated: true)
    }
    
    func start() {
        showLoginFlow()
    }
    
    func authorized() {
        showMainFlow()
    }
    
    func notAuthorized() {
        showLoginFlow()
    }
    
    func showLoginFlow() {
        let loginCoordinator = LoginCoordinator(navigationController)
        loginCoordinator.finishDelegate = self
        VKManager.shared.completeDelegate = self
        loginCoordinator.start()
        childCoordinators.append(loginCoordinator)
    }
    
    func showMainFlow() {
        //OperationsManager.shared.getMyProfile(nil)
        OperationsManager.shared.getFriends(nil)//getAndSaveFriends()
        let tabCoordinator = TabCoordinator(navigationController)
        tabCoordinator.finishDelegate = self
        tabCoordinator.start()
        childCoordinators.append(tabCoordinator)
    }
    
   
}


extension AppCoordinator: CoordinatorFinishDelegate {
    func coordinatorDidFinish(childCoordinator: Coordinator) {
        childCoordinators = childCoordinators.filter( {$0.type != childCoordinator.type} )
        switch childCoordinator.type {
        case .login:
            navigationController.viewControllers.removeAll()
            showMainFlow()
        case .tab:
            navigationController.viewControllers.removeAll()
            showLoginFlow()
        default:
            break
        }
    }
}
