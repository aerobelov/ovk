//
//  AuthViewController.swift
//  oVK
//
//  Created by Pavel Belov on 18.10.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import UIKit
import FirebaseAuth

class AuthViewController: UIViewController {
    
    var didSendEventClosure: ((AuthViewController.Event) -> Void)?
    
    @IBOutlet weak var loginTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var submitButton: UIButton!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func auth() {
        let login = self.loginTextField.text!
        let password = self.passwordTextField.text!
        self.didSendEventClosure?(.login)

    }

}

extension  AuthViewController {
    enum Event {
        case login
    }
}
