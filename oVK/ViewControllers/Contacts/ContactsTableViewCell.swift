//
//  ContactsTableViewCell.swift
//  oVK
//
//  Created by Pavel Belov on 20.10.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import UIKit

class ContactsTableViewCell: UITableViewCell {
    
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var city: UILabel!
    @IBOutlet weak var univer: UILabel!
    @IBOutlet weak var avatar: UIImageView!
    @IBOutlet weak var status: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
       
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
