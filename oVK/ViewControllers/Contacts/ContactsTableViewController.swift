//
//  ContactsTableViewController.swift
//  oVK
//
//  Created by Pavel Belov on 20.10.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import UIKit
import CoreData

class ContactsTableViewController: UITableViewController, FriendsProtocol {
    
    //weak var coordinator: ContactsCoordinator?

    var didSendEventClosure: ((ContactsTableViewController.Event) -> Void)?
    var modelsArray = [NSManagedObject]()
    
    var container: NSPersistentContainer?
 
    func reload () -> Void {
        OperationsManager.shared.getFriends { result in
            
            DispatchQueue.main.async {
                self.modelsArray = result
                self.tableView.reloadData()
            }
            
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        let nibName = UINib(nibName: "ContactsTableViewCell", bundle: nil)
        tableView.register(nibName, forCellReuseIdentifier: "contactCell")
        let manager = CoreDataManager.shared
        manager.presentFriendsDelegate = self
        print("COMPARE SELF MODELS=\(self.modelsArray.count) VS OPS MODESL \(OperationsManager.friends.count)")
        if OperationsManager.friends.count != 0 {
            print("COMPARE SELF MODELS=\(self.modelsArray.count) VS OPS MODESL \(OperationsManager.friends.count)")
            self.modelsArray = OperationsManager.friends
            self.tableView.reloadData()
        } else {
            print("zero")
            reload()
        }
        
        
        
    }
 
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       // reload()
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return modelsArray.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
       let cell = tableView.dequeueReusableCell(withIdentifier: "contactCell", for: indexPath) as! ContactsTableViewCell
        let person = modelsArray[indexPath.row]
        
        let name = person.value(forKey: "last_name") as? String
        let gname = person.value(forKey: "first_name") as? String
        if let name = name, let gname = gname {
            let fullname = "\(name) \(gname)"
            cell.title.text = fullname
        }
        
        cell.city.text = person.value(forKey: "city") as? String
        cell.univer.text = person.value(forKey: "university_name") as? String
        let netstatus = person.value(forKey: "online") as? Int16 == 1 ? "Online" : ""
        cell.status.text = netstatus
        getImage(for: person, in: cell)
     
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
    
    override func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        didSendEventClosure?(.noname)
    }
}

protocol FriendsProtocol {
    var modelsArray: [NSManagedObject] {get set}
    func reload() -> Void 
}

extension  ContactsTableViewController {
    enum Event {
        case contacts, noname
    }
}

extension ContactsTableViewController {
    
    func getImage(for person: NSManagedObject, in cell: ContactsTableViewCell) {
        let urlstring = person.value(forKey: "photo_50") as? String ?? "https://vk.com/images/camera_50.png"
        if let url = URL(string: urlstring) {
            DispatchQueue.global().async {
                let data = try? Data(contentsOf: url)
                DispatchQueue.main.async {
                    cell.avatar.image = UIImage(data: data!)
                }
            }
        }
    }
}
