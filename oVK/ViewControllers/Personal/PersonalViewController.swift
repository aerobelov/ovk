//
//  PersonalViewController.swift
//  oVK
//
//  Created by Pavel Belov on 21.10.2020.
//  Copyright © 2020 Pavel Belov. All rights reserved.
//

import UIKit
import CoreData

class PersonalViewController: UIViewController, FriendsProtocol {
    
    var modelsArray: [NSManagedObject] = []
    
    func reload() {
        OperationsManager.shared.getMyProfile { result in
            
            DispatchQueue.main.async { [self] in
                let urlstring = OperationsManager.me.last?.value(forKey: "photo_200") as? String ?? "https://vk.com/images/camera_50.png"
                if let url = URL(string: urlstring) {
                    DispatchQueue.global().async {
                        let data = try? Data(contentsOf: url)
                        DispatchQueue.main.async {
                            self.personalImage?.image = UIImage(data: data!)
                        }
                    }
                }
                let last = OperationsManager.me.last?.value(forKey: "last_name") as? String ?? "Name"
                let first = OperationsManager.me.last?.value(forKey: "first_name") as? String ?? "First"
                let edu = OperationsManager.me.last?.value(forKey: "university_name") as? String ?? "NOEDU"
                
                self.nameLabel?.text = last + " " + first
                self.cityLabel?.text = OperationsManager.me.last?.value(forKey: "city") as? String
                let univer = OperationsManager.me.last?.value(forKey: "university_name") as? String ?? "No university data"
                let gradu = OperationsManager.me.last?.value(forKey: "graduation") as? Int16 ?? 1977
                self.educationLabel?.text = univer + " '" + String(gradu)
                let friends = OperationsManager.me.last?.value(forKey: "followers") as? Int16 ?? 0
                self.friendsCountLabel?.text = String(friends) + " друзей"
                let networkstatus = OperationsManager.me.last?.value(forKey: "online") as? Int16 ?? 0
                let netw = networkstatus == 1 ? "Online":  "Offline"
                self.networkStatusLabel?.text = netw
                
            }
            
        }
    }
    
    
    var didSendEventClosure: ((PersonalViewController.Event)->Void)?
    @IBOutlet weak var nameLabel: UILabel?
    @IBOutlet weak var networkStatusLabel: UILabel?
    @IBOutlet weak var socialStatusLabel: UILabel?
    @IBOutlet weak var friendsCountLabel: UILabel?
    @IBOutlet weak var subscribersCountLabel: UILabel?
    @IBOutlet weak var cityLabel: UILabel?
    @IBOutlet weak var educationLabel: UILabel?
    @IBOutlet weak var organizationLabel: UILabel?
    @IBOutlet weak var personalImage: UIImageView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let manager = CoreDataManager.shared
        manager.presentFriendsDelegate = self
        reload()
    }
}

extension  PersonalViewController {
    enum Event {
        case caseOne
    }
}

extension UIViewController {
    class func loadFromNib<T: UIViewController>() -> T {
        return T(nibName: String(describing: self), bundle: nil)
    }
}

